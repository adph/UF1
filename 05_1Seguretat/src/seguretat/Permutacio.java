package seguretat;

import java.util.Locale;
import java.util.Scanner;

public class Permutacio {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        String codigo = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        System.out.println("Introduce cadena a encriptar:");
        String mensaje = scan.nextLine();

        /*guardamos el mensaje encriptado, que nos devuelve la funcion
        pasamos por parametro el 'codigo de seguridad' y el mensaje a encriptar
        */
        String mensajeEncriptado = cifrado_Cesar(codigo, mensaje);

        /*guardamos el mensaje desencriptado, que nos devuelve la funcion
        pasamos por parametro el 'codigo de seguridad' y el 'mensajeEncriptado'
        que nos devuelve la funcion 'cifrado_Cesar'
        */
        String mensajeDesencriptado = descrifrado_Cesar(codigo, mensajeEncriptado);

        //obtenemos los resultados por consola
        System.out.println("\nMensaje con Cifrado Cesar: " + mensajeEncriptado);

        System.out.println("\nMensaje con Desencriptado Cesar: " + mensajeDesencriptado);

    }

    public static String cifrado_Cesar(String codigo, String mensaje) {

        String mesanje_salida = "";

        mensaje = mensaje.toUpperCase();

        //recorremos el mensaje introducido
        for (int i = 0; i < mensaje.length(); i++) {

            //al primer caracter del mensaje le incrementamos las posiciones que haya en 'posiciones'
            char caracter = mensaje.charAt(i);

            //nos indica la posicon en la que se encuentra el caracter  ej: 'C' pos = 3
            int pos = codigo.indexOf(caracter);

            //si es menos que 'A' pones el mismo caracter donde este pos
            if (pos == -1) {
                mesanje_salida += caracter;
            }
            //sumamos en la posicion que este 5 posiciones y si ha llegado a la 'Z' con % hacemos que enpieza por la 'A'
            else {
                mesanje_salida += codigo.charAt((pos + 5) % codigo.length());
            }

        }
        return mesanje_salida;
    }

    public static String descrifrado_Cesar(String codigo, String mensaje) {

        String mesanje_salida = "";

        mensaje = mensaje.toUpperCase();

        //recorremos el mensaje introducido
        for (int i = 0; i < mensaje.length(); i++) {

            //al primer caracter del mensaje le incrementamos las posiciones que haya en 'posiciones'
            char caracter = mensaje.charAt(i);

            //nos indica la posicon en la que se encuentra el caracter  ej: 'C' pos = 3
            int pos = codigo.indexOf(caracter);

            //si es menos que 'A' pones el mismo caracter donde este pos
            if (pos == -1) {
                mesanje_salida += caracter;
            } else {
                //si la posicion es menor que 'A'
                if (pos - 5 < 0) {

                    mesanje_salida += codigo.charAt((codigo.length() + (pos - 5)));
                } else {
                    mesanje_salida += codigo.charAt((pos - 5) % codigo.length());
                }

            }

        }
        return mesanje_salida;
    }


}
