package conceptes;

public class Empleat {

    private String nom;
    private double sou;
    private String seccio;

    public Empleat(String nom, double sou) {
        this.nom = nom;
        this.sou = sou;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getSou() {
        return sou;
    }

    public void setSou(double sou) {
        this.sou = sou;
    }

    public String getSeccio() {
        return seccio;
    }

    public void setSeccio(String seccio) {
        this.seccio = seccio;
    }

    public void pujaSou(int percentatge){

        double puja_sou = (getSou() * percentatge)/100;

        System.out.println(puja_sou);
    }

}
