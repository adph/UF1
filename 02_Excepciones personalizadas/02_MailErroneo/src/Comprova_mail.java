import javax.swing.*;

public class Comprova_mail {

    public static void main(String[] args) {

        String el_mail = JOptionPane.showInputDialog("Introduce mail");

        /* se crea un try..catch para controlar que el email cumpla uno delos requisitos
        de la funcion "if (mail.length() < 3)"

         */
        try {
            // llamamos a la funcion
            examina_mail(el_mail);

        }
        // en el caso que no se cumpla
        catch (EmailErroneo e) {

            // se imprime la pila de llamada de objeto "e" de tipo "EmailErroneo" (Excepcion)
            e.printStackTrace();

            // mostramos el mensaje, del tipo de excepcion personalizada.
            System.out.println("El email, tiene que tener como minimo 3 caracteres.");
        }
    }

    static void examina_mail(String mail) throws EmailErroneo {

        int arroba = 0;

        boolean punt = false;

        // creamos la condicion, paa el caso de que el email tenga menos de 3 caracteres
        if (mail.length() < 3) {

            /* creamos un objeto de tipo "EmailErroneo" al cual le pasamos por parametro
            el mensaje que apararecera cuando se lance la excepcion
             */

            // lanzamos la excepcion
            throw new EmailErroneo();


        }

        for (int i = 0; i < mail.length(); i++) {

            if (mail.charAt(i) == '@') {

                arroba++;
            }

            if (mail.charAt(i) == '.') {

                punt = true;
            }
        }


        if (arroba == 1 && punt) {


            System.out.println("És correcte");
        } else {

            System.out.println("No és correcte");
        }
    }
}

