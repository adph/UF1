package conceptes;

import java.util.Date;
import java.util.GregorianCalendar;

public class Empleat {

    private String nom;
    private double sou;
    private String seccio;
    private Date altacontrato;


    public Empleat(String nom, double sou, int any, int mes, int dia) {
        this.nom = nom;
        this.sou = sou;
        seccio = "Administracio";
        GregorianCalendar obtenerFecha = new GregorianCalendar(any, mes, dia);
        altacontrato = obtenerFecha.getTime();
    }

    public Empleat(String nom, double sou, String seccio, int any, int mes, int dia) {
        this.nom = nom;
        this.sou = sou;
        this.seccio = seccio;
        GregorianCalendar obtenerFecha = new GregorianCalendar(any, mes, dia);
        altacontrato = obtenerFecha.getTime();
    }

    public Empleat() {
    }

    public String donamNom() {
        return nom;
    }

    public double donamSou() {
        return sou;
    }

    public String donamSeccio() {
        return seccio;
    }

    public void pujaSou(int percentatge) {

        double incentivoSubido = (sou * percentatge) / 100;

        System.out.println(incentivoSubido);

    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setSou(double sou) {
        this.sou = sou;
    }

    public void setSeccio(String seccio) {
        this.seccio = seccio;
    }

    public String getSeccio() {
        return seccio;
    }

    public Date getAltacontrato() {
        return altacontrato;
    }
}

