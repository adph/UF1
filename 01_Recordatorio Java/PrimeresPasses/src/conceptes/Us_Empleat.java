package conceptes;

public class Us_Empleat {

    public static void main(String[] args) {

        /**
         * Las lineas de codigo conmentadas, corresponden a las pruebas que se han ido haciendo
         * mientras se iba realizando la actividad.
         */

//        Empleat empleado1 = new Empleat("LLuis", 20000);
//        Empleat empleado2 = new Empleat("Anna", 25000);
//        Empleat empleado3 = new Empleat("Marta", 25000);

//        empleado2.pujaSou(5);

//        System.out.println();
        Cap cap1 = new Cap("Alberto", 2000,2005,1,10);

        cap1.estableixIncetiu(1000);

        cap1.setSeccio("Direccio");

//        System.out.println(cap1.donamSou());

//        Empleat[] empleados = new Empleat[3];

//        empleados[0] = new Empleat("LLuis", 20000);
//        empleados[1] = new Empleat("Anna", 25000);
//        empleados[2] = new Empleat("Marta", 25000);
//
//        System.out.println();
//        for (Empleat empleado : empleados) {
//            empleado.pujaSou(5);
//        }

        Empleat[] elsMeusEmpleats = new Empleat[4];

        elsMeusEmpleats[0] = new Empleat("LLuis", 20000,2010,10,23);
        elsMeusEmpleats[1] = new Empleat("Anna", 25000,2012,7,14);
        elsMeusEmpleats[2] = new Empleat("Marta", 25000,2018,3,12);
        elsMeusEmpleats[3] = cap1;

        System.out.println();
        for (Empleat empleado : elsMeusEmpleats) {
            System.out.println("Empleado: " + empleado.donamNom() + "\n" +
                    "Tiene un sueldo de: " + empleado.donamSou() + "\n" +
                    "Pertenece a seccion: " + empleado.getSeccio() + "\n" +
                    "Fecha de alta: " + empleado.getAltacontrato());
        }
    }
}
