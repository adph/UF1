package conceptes;

public class Cap extends Empleat {

    private double incentiu;

    public Cap(String nom, double sou, int any, int mes, int dia) {
        super(nom, sou, any, mes, dia);
    }

    public void estableixIncetiu(double incen) {

        incentiu = incen;
    }

    public double donamSou() {
        /* Al poner "super" delante del metodo, estamos forzando a que use
         el metodo de la clase "Cap" y no el que hereda de "Empleat"
        */
        double sueldoBaseJefe = super.donamSou();

        return sueldoBaseJefe + incentiu;
    }

}
