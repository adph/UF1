import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ConBotones2 {

    public static void main(String[] args) {

        MarcoCliente mimarco = new MarcoCliente();
        mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mimarco.setTitle("Con Botones");
    }
}

class MarcoCliente extends JFrame {
    public MarcoCliente() {
        setBounds(600, 300, 280, 350);
        LaminaMarcoCliente milamina = new LaminaMarcoCliente();
        add(milamina);
        setVisible(true);
    }
}

class LaminaMarcoCliente extends JPanel implements ActionListener {

    public LaminaMarcoCliente() {
        setLayout(null);

        // construimos, posicionamos y agregamos la etiqueta Label "jlCliente"
        JLabel jlCliente = new JLabel("CLIENTE");

        jlCliente.setBounds(10, 10, 50, 10);

        //Asignamos un tipo de letra a Label "jlCliente"
        jlCliente.setFont(new Font("Arial", Font.BOLD, 12));
        add(jlCliente);

        // construimos, posicionamos y agregamos los dos TextFields
        tfCliente = new TextField();
        tfInferior = new TextField();

        tfCliente.setBounds(60, 5, 180, 20);
        tfInferior.setBounds(60, 30, 180, 20);

        add(tfCliente);
        add(tfInferior);

        // construimos, posicionamos y agregamos los botones
        miBotonVerde = new JButton("Verde");
        miBotonAmarillo = new JButton("Amarillo");
        miBotonNaranja = new JButton("Naranja");

        miBotonVerde.setBounds(90, 60, 100, 30);
        miBotonAmarillo.setBounds(90, 100, 100, 30);
        miBotonNaranja.setBounds(90, 140, 100, 30);

        add(miBotonVerde);
        add(miBotonAmarillo);
        add(miBotonNaranja);

        // accion al pulsar el boton
        miBotonVerde.addActionListener(this);
        miBotonAmarillo.addActionListener(this);
        miBotonNaranja.addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        //recogemos el contenido de "tfCliente"
        String recogerTexto = tfCliente.getText();

        //cambiamos el contenido de "tfCliente""
        tfCliente.setText("");

        //introducimos el contenido de "recogeTexto"
        tfInferior.setText(recogerTexto);

        // se bloquea TextField "tfInferior" para que no se pueda modificar el texto
        tfInferior.setEnabled(false);


        //condicional para cambiar el color de fondo de la "lamina"
        if (e.getSource().equals(miBotonVerde)) {
            setBackground(Color.GREEN);

        } else if (e.getSource().equals(miBotonAmarillo)) {
            setBackground(Color.YELLOW);
        } else {
            setBackground(Color.ORANGE);
        }


    }

    //variable locales
    private final JButton miBotonVerde;
    private final JButton miBotonAmarillo;
    private final JButton miBotonNaranja;
    private final TextField tfCliente;
    private final TextField tfInferior;

}
