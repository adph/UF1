import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CifradoCypher {

    public static void main(String[] args) {

        Marco mimarco = new Marco();
        mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mimarco.setTitle("Cifrado__Cypher");
    }


    static class Marco extends JFrame {
        public Marco() {
            setBounds(400, 200, 800, 500);
            LaminaMarco milamina = new LaminaMarco();
            add(milamina);
            setVisible(true);
        }
    }

    static class LaminaMarco extends JPanel implements ActionListener {

        public LaminaMarco() {
            setLayout(null);

            //construimos, posicionamos y agregamos las etiquetas
            JLabel jlOriginal = new JLabel("ORIGINAL");
            JLabel jlEncriptar = new JLabel("ENCRIPTAR");
            JLabel jlDesencriptar = new JLabel("DESENCRIPTAR");

            jlOriginal.setBounds(30, 20, 100, 30);
            jlEncriptar.setBounds(30, 120, 100, 30);
            jlDesencriptar.setBounds(30, 220, 100, 30);

            //asignamos un tipo de letra
            jlOriginal.setFont(new Font("Arial", Font.BOLD, 12));
            add(jlOriginal);
            jlEncriptar.setFont(new Font("Arial", Font.BOLD, 12));
            add(jlEncriptar);
            jlDesencriptar.setFont(new Font("Arial", Font.BOLD, 12));
            add(jlDesencriptar);

            //construimos, posicionamos y agregamos los dos TextFields
            tfOriginal = new TextField();
            tfEncriptar = new TextField();
            tfDesencriptar = new TextField();

            tfOriginal.setBounds(10, 50, 760, 50);
            tfEncriptar.setBounds(10, 150, 760, 50);
            tfDesencriptar.setBounds(10, 250, 760, 50);

            add(tfOriginal);
            add(tfEncriptar);
            add(tfDesencriptar);

            //construimos, posicionamos y agregamos los botones
            btEncriptar = new JButton("ENCRIPTAR");
            btDesencriptar = new JButton("DESENCRIPTAR");

            btEncriptar.setBounds(250, 400, 150, 30);
            btDesencriptar.setBounds(430, 400, 150, 30);

            add(btEncriptar);
            add(btDesencriptar);

            //accion al pulsar el boton
            btEncriptar.addActionListener(this);
            btDesencriptar.addActionListener(this);

            tfEncriptar.setEnabled(false);
            tfDesencriptar.setEnabled(false);
            btDesencriptar.setEnabled(false);

        }

        @Override
        public void actionPerformed(ActionEvent e) {

            //recogemos el contenido de "tfOriginal"
            String recogerTexto = tfOriginal.getText();

            //ponemos el contenido del texto encriptado en "tfEncriptado""
            String textoEncriptado = (encriptar(recogerTexto));

            //recogemos el contenido de la clave encriptada en "textoDesencriptado"
            String textoDesencriptado = (desencriptar(recogerTexto));

            //si pulsamos el boton 'btEncriptar' mostramos el contenido de "textoEncriptado"

            if (e.getSource().equals(btEncriptar)) {
                btDesencriptar.setEnabled(false);
                tfEncriptar.setText(textoEncriptado);
                btDesencriptar.setEnabled(true);
            }
            //mostramos el contenido de "textoDesencriptado"
            else tfDesencriptar.setText(textoDesencriptado);

        }

        private String encriptar(String mensaje) {

            byte[] textXifrat = new byte[0];

            try {

                KeyGenerator keygen = KeyGenerator.getInstance("AES");
                SecretKey key = keygen.generateKey();

                Cipher aesCipher = Cipher.getInstance("AES");
                aesCipher.init(Cipher.ENCRYPT_MODE, key);

                // converteix el string a byte[] i xifra el missatge
                byte[] text = mensaje.getBytes("UTF-8");
                textXifrat = aesCipher.doFinal(text);

                String missatgeXifrat = new String(textXifrat);
//            System.out.println(missatgeXifrat);

                // converteix el missatge desxifrat a byte[] i xifra el missatge

                aesCipher.init(Cipher.DECRYPT_MODE, key);
                byte[] textDesxifrat = aesCipher.doFinal(textXifrat);

//            System.out.println(new String(textDesxifrat));

            } catch (Exception e) {
                e.printStackTrace();
            }

            return (new String(textXifrat));

        }

        private String desencriptar(String mensaje) {

            // converteix el missatge desxifrat a byte[] i xifra el missatge

            byte[] textDesxifrat = new byte[0];


            try {

                KeyGenerator keygen = KeyGenerator.getInstance("AES");
                SecretKey key = keygen.generateKey();

                Cipher aesCipher = Cipher.getInstance("AES");
                aesCipher.init(Cipher.ENCRYPT_MODE, key);

                // converteix el string a byte[] i xifra el missatge
                byte[] text = mensaje.getBytes("UTF-8");
                byte[] textXifrat = aesCipher.doFinal(text);

                String missatgeXifrat = new String(textXifrat);
//            System.out.println(missatgeXifrat);

                // converteix el missatge desxifrat a byte[] i xifra el missatge

                aesCipher.init(Cipher.DECRYPT_MODE, key);
                textDesxifrat = aesCipher.doFinal(textXifrat);
//            System.out.println(new String(textDesxifrat));

                return (new String(textDesxifrat));

            } catch (Exception e) {
                e.printStackTrace();
            }
            return (new String(textDesxifrat));

        }


        //variable locales
        private final JButton btEncriptar;
        private final JButton btDesencriptar;
        private final TextField tfOriginal;
        private final TextField tfEncriptar;
        private final TextField tfDesencriptar;

    }
}
